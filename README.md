# Hackathon Update Sets

This repo contains Update Sets for the capabiltiies the Hackathon. They can be installed into an instance by importing in the XML file using the standard platform capabilties.

Folder | Description
---|--
cicd_bootstrap | CI/CD Bootstrap
cicd_git_push_update_set | Git Push
incident_modifications | Incident Sample App
sn_appclient | SN App Repo Client Extensions
x_86532_deployment | Deployment Manager Application

## CI/CD Bootstrap

Provides REST APIs that enables the following deployment activities:

- Plugin Activitation
- Update Set Preview, Problem Resolution and Commit actions

## Git Push

Prototype for adding any Update Set to a Git Repository.

Uses Orchestration to have a MID Server download the Update Set from the instance, and then add it to a local git repo. The Orchestration script then writes commits it to the local repository and pushes it to the remote repo.

Would require additional work to:

- Allow developer to write custom commit message
- Parameterize the integration to allow the remote repo to be configured, as well as the credentials that should be used

## Incident Sample App

A small sample application that extends Incident Management, used to demonstrate the deployment concepts.


## SN App Repo Client Extensions

Exposes the App Repository Client capabiltiies used by ServiceNow pages as prototype REST APIs.

Would require additional work to:

- Handle edge cases currently handled by logic in the pages


## Deployment Manager Application

Provide the capability to define a deployment manifest. Currently supports the capability to handle plugin activation, update set deployment and git-based app deployment.

Also contains a sample orchestration workflow for deploying a package to a target instance.

Would require additional work to:

- Provide REST APIs for registration of new deployable artifacts (Update Set, Application Versions)
- Handle App Repo deployment
- Ability to generate or parse manifest file to be stored in source control
- Cross-package depedency management